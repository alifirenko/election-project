package Elections.Users;

import Elections.Elections;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Map;

public class ConnectorUsers {

    public void addUser (JSONObject user) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject object = (JSONObject) parser.parse(user.toJSONString());
            String name = (String) object.get("name");
            String secondName = (String) object.get("secondName");
            String middleName = (String) object.get("middleName");
            String birthday = (String) object.get("birthday");
            String strit = (String) object.get("strit");
            String house = (String) object.get("house");
            String room = (String) object.get("room");
            String login = (String) object.get("login");
            String password = (String) object.get("password");
            if (!middleName.isEmpty() && !room.isEmpty()) {
                Users userDate = new Users(name, secondName, middleName, birthday, strit, house, room, login, password);
            } else if (!middleName.isEmpty() && room.isEmpty()) {
                Users userDate = new Users(name, secondName, middleName, birthday, strit, house, login, password);
            } else if (middleName.isEmpty() && !room.isEmpty()) {
                Users userDate = new Users(name, secondName, birthday, strit, house, room, login, password);
            } else {
                Users userDate = new Users(name, secondName, birthday, strit, house, login, password);
            }
        } catch (ParseException e) {
            Elections.writeFileLog("Parsing info user");
        }
    }

    public Map.Entry<JSONObject, Integer> getUserOnLoginAndPassword (String login, String password) {
        Users user = new Users();
        JSONParser parser = new JSONParser();
        for (Map.Entry<JSONObject, Integer> entry : user.getUsers().entrySet()) {
            try {
                JSONObject object = (JSONObject) parser.parse(entry.getKey().toJSONString());
                String loging = (String) object.get("login");
                String pass = (String) object.get("password");
                if (loging.equals(login) && pass.equals(password)) {
                    return entry;
                }
            } catch (ParseException e) {
                Elections.writeFileLog("Get User in filed. Login or password an found");
            }
        }
        return null;
    }

    public Map.Entry<JSONObject, Integer> getUserByrqUID (Integer rqUID) {
        Users user = new Users();
        JSONParser parser = new JSONParser();
        for (Map.Entry<JSONObject, Integer> entry : user.getUsers().entrySet()) {
            try {
                JSONObject object = (JSONObject) parser.parse(entry.getKey().toJSONString());
                Integer uid = (Integer) object.get("rqUID");
                if (rqUID.equals(uid)) {
                    return entry;
                }
            } catch (ParseException e) {
                Elections.writeFileLog("Get User in filed. User not found");
            }
        }
        return null;
    }

}

