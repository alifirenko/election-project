package Elections.Users;

import org.json.simple.JSONObject;

import java.util.HashMap;

public class Users {
    private HashMap<JSONObject, Integer> users;
    private Integer count;

    public Users (String firstName, String secondName, String middleName, String birthday, String strit, String house, String room, String login, String password) {
        JSONObject user = new JSONObject();
        user.put("name", firstName);
        user.put("family", secondName);
        user.put("middleName", middleName);
        user.put("birthday", birthday);
        user.put("strit", strit);
        user.put("house", house);
        user.put("room", room);
        user.put("login", login);
        user.put("password", password);
        user.put("acountStatus", true);
        user.put("candidateStatus", false);
        user.put("rqUID", count++);
        users.put(user, checkCount());
    }

    public Users(String firstName, String secondName, String birthday, String strit, String house, String room, String login, String password) {
        JSONObject user = new JSONObject();
        user.put("name", firstName);
        user.put("family", secondName);
        user.put("birthday", birthday);
        user.put("strit", strit);
        user.put("house", house);
        user.put("room", room);
        user.put("login", login);
        user.put("password", password);
        user.put("acountStatus", true);
        user.put("candidateStatus", false);
        user.put("rqUID", count++);
        users.put(user, checkCount());
    }

    public Users(String firstName, String secondName, String birthday, String strit, String house, String login, String password) {
        JSONObject user = new JSONObject();
        user.put("name", firstName);
        user.put("family", secondName);
        user.put("birthday", birthday);
        user.put("strit", strit);
        user.put("house", house);
        user.put("login", login);
        user.put("password", password);
        user.put("acountStatus", true);
        user.put("candidateStatus", false);
        user.put("rqUID", count++);
        users.put(user, checkCount());
    }

    public Users() {

    }

    private Integer checkCount () {
        count++;
        return count;
    }

    public HashMap<JSONObject, Integer> getUsers() {
        return users;
    }

    private void setUsers(HashMap<JSONObject, Integer> users) {
        this.users = users;
    }
}