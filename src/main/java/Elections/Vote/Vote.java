package Elections.Vote;

import Elections.Candidates.Nomination;
import Elections.Elections;

import java.util.Map;

/**
 * Created by SBT-Alifirenko-IV on 11.01.2016.
 */
public class Vote {
    private Map<Nomination, Integer> vote;

    public Vote(Map<Nomination, Integer> vote) {
        this.vote = vote;
    }

    public Map<Nomination, Integer> getVote() {
        return vote;
    }

    public void setVoteCount(Nomination nominant) {
        vote.put(nominant, this.vote.get(nominant) + 1);
    }
}
