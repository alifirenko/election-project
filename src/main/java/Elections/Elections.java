package Elections;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Elections {
    public static Map<String, String> stash = new HashMap<String, String>();
    private static String fileConfiguration = "";

    public static void main(String[] args) {
        Scanner settingScaner = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            fileConfiguration = reader.readLine();
        } catch (IOException e) {
            writeFileLog("Имя файла конфигурации задано не верно");
        }
        finally {
            if (null != reader)
                try {
                    reader.close();
                } catch (IOException e) {
                    writeFileLog("Поток чтения имеи файла конфигурации был завершен не корректно");
                }
        }
        try {
            settingScaner = new Scanner(new File(fileConfiguration).getAbsoluteFile());
            while (settingScaner.hasNext()) {
                String line = settingScaner.nextLine();
                String[] buffer = split(line);
                stash.put(buffer[0], buffer[1]);
            }
        } catch (FileNotFoundException e) {
            writeFileLog("Файл конфигурации не найден");
        }
        finally {
            if (null != settingScaner)
                settingScaner.close();
        }
    }

    public static String[] split (String line) {
        String[] value = new String[2];
        value = line.split("=");
        return value;
    }

    public static void writeFileLog (String writeLog) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(new File("log.txt").getAbsoluteFile());
            writer.write(writeLog);
        } catch (IOException e) {
            writeFileLog(e.toString());
        }
        finally {
            if (null != writer) {
                try {
                    writer.close();
                } catch (IOException e) {
                    writeFileLog(e.toString());
                }
            }
        }
    }
}
