package Elections.Offers;

import Elections.Elections;
import Elections.Users.ConnectorUsers;
import Elections.Users.Users;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Set;

public class OfferConnector {

    public void addOffer (JSONObject offer) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject object = (JSONObject) parser.parse(offer.toJSONString());
            Integer uidUser = (Integer) object.get("rqUID");
            String message = (String) object.get("message");
            Offering offering = new Offering((Users) new ConnectorUsers().getUserByrqUID(uidUser), message);
        } catch (ParseException e) {
            Elections.writeFileLog("Error add offer");
        }
    }

    public void addStatusInOffer (JSONObject offer) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject object = (JSONObject) parser.parse(offer.toJSONString());
            Integer uidUser = (Integer) object.get("rqUID");
            String message = (String) object.get("offer");
            Boolean status = (Boolean) object.get("status");
            Offering offering = new Offering((Users) new ConnectorUsers().getUserByrqUID(uidUser), message, status);
        } catch (ParseException e) {
            Elections.writeFileLog("Offer not found");
        }
    }

    public void deleteStatusInOffer (JSONObject offer) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject object = (JSONObject) parser.parse(offer.toJSONString());
            Integer uidUser = (Integer) object.get("rqUID");
            String message = (String) object.get("offer");
            JSONObject newObject = getOffer((Users) new ConnectorUsers().getUserByrqUID(uidUser), message);

            object = (JSONObject) parser.parse(newObject.toJSONString());
            Boolean newLike  = (Boolean) object.get("like");
            Boolean newDisLike = (Boolean) object.get("dislike");
            if (newLike.equals(true)) {
                newObject.put("like", false);
            } else if (newDisLike.equals(true)) {
                newObject.put("dislike", false);
            }
            new Offering().getOffers().remove(newObject);
            addOffer(object);
        } catch (ParseException e) {
            Elections.writeFileLog("Offer not found");
        }
    }

    public JSONObject getOffer (Users user, String message) {
        Offering offering = new Offering();
        Set<JSONObject> offers = offering.getOffers();
        JSONParser parser = new JSONParser();
        for (JSONObject buffer : offers) {
            try {
                JSONObject object = (JSONObject) parser.parse(buffer.toJSONString());
                return buffer;
            } catch (ParseException e) {
                Elections.writeFileLog(e.toString());
            }
        }
        return null;
    }
}