package Elections.Offers;

import Elections.Elections;
import Elections.Users.ConnectorUsers;
import Elections.Users.Users;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Set;
import java.util.TreeSet;

public class Offering {
    private Users user;
    private String offer;
    private int like;
    private int dislike;
    private Set<JSONObject> offers = new TreeSet<JSONObject>();

    public Offering(Users user, String offer) {
        JSONObject bufOffer = new JSONObject();
        bufOffer.put("user", user);
        bufOffer.put("offer", offer);
        bufOffer.put("like", false);
        bufOffer.put("dislike", false);
        offers.add(bufOffer);
    }

    public Offering(Users user, String message, Boolean status) {
        JSONParser parser = new JSONParser();
        for (JSONObject buffer : offers) {
            try {
                JSONObject object = (JSONObject) parser.parse(buffer.toJSONString());
                Integer userOffer = (Integer) object.get("user");
                StringBuilder offerMessage = (StringBuilder) object.get("offer");
                if (user.equals(userOffer) && offerMessage.equals(offerMessage)) {
                    if (status != false) {
                        buffer.put("like", true);
                    } else {
                        buffer.put("dislike", true);
                    }
                }
            } catch (ParseException e) {
                Elections.writeFileLog(e.toString());
            }
        }
    }

    public Offering() {
    }

    public Set<JSONObject> getOffers() {
        return offers;
    }
}
