package Elections.Candidates;

import Elections.Elections;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.HashMap;

public class ConnectorCandidates {

    public void addCandidate (JSONObject user) {
        Nomination candidate = new Nomination(user);
    }

    public void deleteCandidate (JSONObject user) {
        Nomination candidate = new Nomination();
        candidate.getCandidates().remove(user);
    }

    public void setAgreeStatus (JSONObject user, Boolean agreeStatus) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject object = (JSONObject) parser.parse(user.toJSONString());
            Boolean agreeStatusInJSON = (Boolean) object.get("candidateStatus");
            object.put("candidateStatus", agreeStatus);
            deleteCandidate(user);
            Nomination candidate = new Nomination(object);
        } catch (ParseException e) {
            Elections.writeFileLog("Error in parse User in Candidates");
        }
    }

    public void setEligibilityStatus(JSONObject user, Boolean eligibilityStatus) {
        Nomination candidate = new Nomination();
        HashMap<JSONObject, Boolean> bufferObject = new HashMap<JSONObject, Boolean>();
        bufferObject = candidate.getCandidates();
        bufferObject.put(user, eligibilityStatus);
        candidate.setCandidates(bufferObject);
    }

    public HashMap getCandidates () {
        Nomination candidate = new Nomination();
        return candidate.getCandidates();
    }
}
